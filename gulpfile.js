var path = require('path'),
	gulp = require('gulp'),
	minifycss = require('gulp-minify-css'),
	uglify = require('gulp-uglify'),
	concat = require('gulp-concat'),
	del = require('del'),
	rename = require('gulp-rename'),
	angularTemplateCache = require('gulp-angular-templatecache'),
	addStream = require('add-stream'),
	dest = 'dist';

gulp.task('default', ['watch', 'styles', 'scripts']);

gulp.task('clean', function() {
	return del([dest + '/**/*', '!' + dest + '/.gitignore']);
});

gulp.task('watch', function() {
	gulp.watch('css/**/*.css', ['styles']);
	gulp.watch(['js/**/*.js', 'templates/**/*.html'], ['scripts']);
});

gulp.task('styles', function() {
	gulp
		.src([
			'css/base.css',
			'css/bootstrap-overwrite.css',
			'css/app.css'
		])
		.pipe(concat('fileman.css'))
		.pipe(gulp.dest(dest))
		.pipe(minifycss({
			keepSpecialComments: 0,
			rebase: true,
			relativeTo: path.join(process.cwd(), dest),
			target: path.join(process.cwd(), dest)
		}))
		.pipe(rename({
			suffix: '.min'
		}))
		.pipe(gulp.dest(dest));
});

gulp.task('scripts', function() {
	gulp
		.src(
			[
				'js/app.js',
				'js/bootstrap.js',
				'js/routes.js',
				'js/controllers/*.js',
				'js/directives/*.js',
				'js/filters/*.js',
				'js/services/*.js'
			]
		)
		.pipe(addStream.obj(prepareTemplates()))
		.pipe(concat('fileman.js'))
		.pipe(gulp.dest(dest))
		.pipe(uglify({
			preserveComments: 'some'
		}))
		.pipe(rename({
			suffix: '.min'
		}))
		.pipe(gulp.dest(dest));

	// manager
	gulp
		.src('js/manager.js')
		.pipe(rename('fileman.manager.js'))
		.pipe(gulp.dest(dest))
		.pipe(uglify({
			preserveComments: 'some'
		}))
		.pipe(rename({
			suffix: '.min'
		}))
		.pipe(gulp.dest(dest));
});

function prepareTemplates() {
	return gulp.src('templates/**/*.html')
		.pipe(angularTemplateCache('templates.js', {
			root: 'templates/',
			module: 'fileman'
		}));
}