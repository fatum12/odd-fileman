<?php
require 'vendor/autoload.php';

use Fatum12\Fileman\Fileman;

$fileman = new Fileman(__DIR__ . '/files', [
	'uploadMaxFileSize' => 1 * 1024 * 1024,			// 1 MB
	'uploadFileTypes' => '/\.(png|jpe?g)$/i',
	'imageMaxWidth' => 1000,
	'imageMaxHeight' => 1000
]);
$fileman->handleRequest();