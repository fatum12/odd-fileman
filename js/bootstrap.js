(function() {
	'use strict';

	var externalConfig = {},
		parentWindow;

	// get config from global object
	if (window.FILEMAN_CONFIG && angular.isObject(window.FILEMAN_CONFIG)) {
		angular.extend(externalConfig, window.FILEMAN_CONFIG);
	}

	// is child window or iframe?
	parentWindow = window.opener ? window.opener : (window.parent ? window.parent : null);
	if (parentWindow !== null && parentWindow.filemanManager) {
		// get instance id
		var id = getQueryParameter('filemanId');
		// get config from parent window
		if (parentWindow.filemanManager.getConfig(id) !== null) {
			angular.extend(externalConfig, parentWindow.filemanManager.getConfig(id));
		}
	}

	if (!externalConfig.hasOwnProperty('connectorUrl')) {
		throw new Error('Connector url not defined');
	}

	deferredBootstrapper.bootstrap({
		element: document.body,
		module: 'fileman',
		resolve: {
			EXTERNAL_CONFIG: ['$http', function ($http) {
				return $http.get(externalConfig.connectorUrl, {
					params: {
						cmd: 'config'
					}
				})
					.then(function(response) {
						angular.extend(externalConfig, response.data);
						return externalConfig;
					}, function() {
						throw new Error("Can't load settings from server. Check connector url.");
					});
			}]
		}
	});

	function getQueryParameter(name) {
		name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
		var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
			results = regex.exec(location.search);
		return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
	}
})();