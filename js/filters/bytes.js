(function() {
	'use strict';

	angular.module('fileman')
		.filter('bytes', function() {
			return function(bytes, precision) {
				if (bytes == 0 || isNaN(parseFloat(bytes)) || !isFinite(bytes)) return '';
				if (typeof precision === 'undefined') precision = 2;
				if (bytes < 1024) {
					precision = 0;
				}
				var units = ['B', 'KB', 'MB', 'GB', 'TB', 'PB'],
					number = Math.floor(Math.log(bytes) / Math.log(1024));
				return (bytes / Math.pow(1024, Math.floor(number))).toFixed(precision) +  ' ' + units[number];
			}
		});
})();