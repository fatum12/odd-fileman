(function() {
	'use strict';

	angular.module('fileman')
		.factory('dialogs', ['$q', '$uibModal', function($q, $uibModal) {
			return {
				mkdir: function() {
					return $uibModal
						.open({
							templateUrl: 'templates/mkdirDialog.html',
							controller: 'MkdirDialogController',
							controllerAs: 'dialog',
							animation: false
						})
						.result
				},
				rename: function(options) {
					return $uibModal
						.open({
							templateUrl: 'templates/renameDialog.html',
							controller: 'RenameDialogController',
							controllerAs: 'dialog',
							animation: false,
							resolve: {
								options: function () {
									return options;
								}
							}
						})
						.result;
				},
				upload: function(options) {
					return $uibModal
						.open({
							templateUrl: 'templates/uploadDialog.html',
							controller: 'UploadDialogController',
							controllerAs: 'dialog',
							animation: false,
							resolve: {
								options: function () {
									return options;
								}
							}
						})
						.result;
				},
				yesNo: function(options) {
					return $uibModal
						.open({
							templateUrl: 'templates/yesNoDialog.html',
							controller: 'YesNoDialogController',
							controllerAs: 'dialog',
							animation: false,
							resolve: {
								options: function () {
									return options;
								}
							}
						})
						.result;
				},
				image: function(options) {
					$.magnificPopup.open({
						items: options.images.map(function(image) {
							return {
								src: image.viewUrl(),
								title: image.name
							};
						}),
						type: 'image',
						gallery: {
							enabled: true
						}
					}, options.index);
				},
				textView: function(options) {
					return $uibModal
						.open({
							templateUrl: 'templates/textViewDialog.html',
							controller: 'TextViewDialogController',
							controllerAs: 'dialog',
							size: 'lg',
							animation: false,
							resolve: {
								file: function () {
									return options.file;
								}
							}
						})
						.result;
				},
				tree: function(options) {
					return $uibModal
						.open({
							templateUrl: 'templates/treeDialog.html',
							controller: 'TreeDialogController',
							controllerAs: 'dialog',
							animation: false,
							resolve: {
								options: function () {
									return options;
								}
							}
						})
						.result;
				},
				resize: function(options) {
					return $uibModal
						.open({
							templateUrl: 'templates/resizeDialog.html',
							controller: 'ResizeDialogController',
							controllerAs: 'dialog',
							animation: false,
							resolve: {
								options: function () {
									return options;
								}
							}
						})
						.result;
				},
				fileInfo: function(options) {
					return $uibModal
						.open({
							templateUrl: 'templates/fileInfoDialog.html',
							controller: 'FileInfoDialogController',
							controllerAs: 'dialog',
							animation: false,
							resolve: {
								options: function () {
									return options;
								}
							}
						})
						.result;
				},
				zip: function() {
					return $uibModal
						.open({
							templateUrl: 'templates/zipDialog.html',
							controller: 'ZipDialogController',
							controllerAs: 'dialog',
							animation: false
						})
						.result
				}
			};
		}]);
})();