(function() {
	'use strict';

	angular.module('fileman')
		.factory('filesManager', ['$window', '$http', '$filter', 'config', 'File', 'notifications', 'textHelper', 'urlHelper',
			function($window, $http, $filter, config, File, notifications, textHelper, urlHelper) {
				var files = [];

				return {
					getFiles: function() {
						return files;
					},
					getImages: function() {
						return files.filter(function(file) {
							return file.isImage();
						});
					},
					// add new file to files list
					add: function(fileData) {
						var file = new File(fileData)
						files.push(file);
						this.reorder();

						return file;
					},
					// resort files list
					reorder: function() {
						angular.copy($filter('orderBy')(files, config.get('order')), files);
					},
					// list directory
					ls: function(path) {
						return $http.get(config.get('connectorUrl'), {
							params: {
								cmd: 'ls',
								path: path
							}
						}).then(function(response) {
							var filesObjects = response.data.map(function(fileData) {
								return new File(fileData);
							});
							angular.copy($filter('orderBy')(filesObjects, config.get('order')), files);

							return files;
						});
					},
					// make directory
					mkdir: function(path, directoryName) {
						var self = this;

						return $http.post(config.get('connectorUrl'), {
							name: directoryName,
							path: path
						}, {
							params: {
								cmd: 'mkdir'
							}
						}).then(function(response) {
							notifications.success("Created directory '" + response.data.name + "'");
							return self.add(response.data);
						});
					},
					delete: function(items) {
						return $http.post(config.get('connectorUrl'), {
							items: items
						}, {
							params: {
								cmd: 'delete'
							}
						}).then(function(response) {
							notifications.success('Deleted ' + textHelper.pluralize(items.length, ['item', 'items'], true));
						});
					},
					copy: function(items, target) {
						return $http.post(config.get('connectorUrl'), {
							items: items,
							target: target
						}, {
							params: {
								cmd: 'copy'
							}
						}).then(function(response) {
							notifications.success('Copied ' + textHelper.pluralize(items.length, ['item', 'items'], true) +
								" to '" + urlHelper.basename(target) + "'");
						});
					},
					move: function(items, target) {
						return $http.post(config.get('connectorUrl'), {
							items: items,
							target: target
						}, {
							params: {
								cmd: 'move'
							}
						}).then(function(response) {
							notifications.success('Moved ' + textHelper.pluralize(items.length, ['item', 'items'], true) +
								" to '" + urlHelper.basename(target) + "'");
						});
					},
					download: function(file) {
						$window.location = file.downloadUrl();
					},
					zip: function(items, path, archiveName) {
						var self = this;

						return $http.post(config.get('connectorUrl'), {
							items: items,
							path: path,
							archiveName: archiveName
						}, {
							params: {
								cmd: 'zip'
							}
						}).then(function(response) {
							var file = self.add(response.data);
							notifications.success("Created archive '" + file.name + "'");

							return file;
						});
					},
					getSelection: function() {
						return files.filter(function(file) {
							return file.isSelected();
						});
					},
					deselectAll: function() {
						angular.forEach(files, function(file) {
							file.deselect();
						});
					},
					selectionSize: function() {
						return this.getSelection().length;
					},
					hasSelection: function() {
						return this.selectionSize() > 0;
					},
					hasMultiselection: function() {
						return this.selectionSize() > 1;
					},
					hasSingleFileSelected: function() {
						var selected = this.getSelection(),
							first;
						if (selected.length == 1) {
							first = selected[0];
							if (!first.isDir) {
								return true;
							}
						}
						return false;
					},
					hasSingleImageSelected: function() {
						var selected = this.getSelection(),
							first;
						if (selected.length == 1) {
							first = selected[0];
							if (first.isImage()) {
								return true;
							}
						}
						return false;
					},
					hasSingleArchiveSelected: function() {
						var selected = this.getSelection(),
							first;
						if (selected.length == 1) {
							first = selected[0];
							if (first.isArchive()) {
								return true;
							}
						}
						return false;
					}
				};
			}]);
})();