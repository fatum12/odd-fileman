(function($) {
	'use strict';

	angular.module('fileman')
		.factory('notifications', [function() {
			function showAlert(text, type) {
				return $.notify({
					message: text
				}, {
					type: type || 'info',
					allow_dismiss: true,
					newest_on_top: true,
					delay: 6000,
					z_index: 1060
				});
			}

			return {
				success: function(text) {
					showAlert(text, 'success');
				},
				error: function(text) {
					showAlert(text, 'danger');
				}
			};
		}]);
})(jQuery);