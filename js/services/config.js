(function() {
	'use strict';

	angular.module('fileman')
		.factory('config', ['$http', 'EXTERNAL_CONFIG', function($http, EXTERNAL_CONFIG) {
			var config = {},
				obj = {
					get: function(key, defaultValue) {
						if (typeof key == 'undefined') {
							return config;
						}
						if (typeof defaultValue == 'undefined') {
							defaultValue = null;
						}
						if (config.hasOwnProperty(key)) {
							return config[key];
						}
						return defaultValue;
					},
					set: function(key, value) {
						if (angular.isObject(key)) {
							angular.extend(config, key);
						} else {
							config[key] = value;
						}
					}
				};

			obj.set(EXTERNAL_CONFIG);

			return obj;
		}]);
})();