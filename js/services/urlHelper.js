(function() {
	'use strict';

	angular.module('fileman')
		.factory('urlHelper', ['$httpParamSerializer', function($httpParamSerializer) {
			return {
				getQueryDelimiter: function(query) {
					return query.indexOf('?') != -1 ? '&' : '?';
				},
				buildUrl: function(base, params) {
					return base + this.getQueryDelimiter(base) + $httpParamSerializer(params);
				},
				basename: function(path) {
					path = path
						.replace(/\\/g, '/')
						.replace(/\/+$/g, '');
					var parts = path.split('/');
					return parts[parts.length - 1] !== '' ? parts[parts.length - 1] : 'home';
				}
			};
		}]);
})();