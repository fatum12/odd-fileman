(function() {
	'use strict';

	angular.module('fileman')
		.factory('httpErrorHandler', ['$q', 'notifications', function($q, notifications) {
			return {
				responseError: function(rejection) {
					if (rejection.hasOwnProperty('data') && rejection.data.length) {
						notifications.error(rejection.data);
					}

					return $q.reject(rejection);
				}
			};
		}]);
})();