(function() {
	'use strict';

	angular.module('fileman')
		.factory('File', ['$http', 'config', 'urlHelper', 'notifications',
			function($http, config, urlHelper, notifications) {
				function File(data) {
					if (data) {
						this.setData(data);
					}
				}

				var p = File.prototype;

				p.setData = function(data) {
					angular.extend(this, data);
					if (!this.hasOwnProperty('_isSelected')) {
						this._isSelected = false;
					}
				};

				p.load = function(path) {
					var self = this;

					return $http.get(config.get('connectorUrl'), {
						params: {
							cmd: 'stat',
							path: path
						}
					}).then(function(response) {
						self.setData(response.data);
					});
				};

				p.delete = function() {
					var self = this;

					return $http.post(config.get('connectorUrl'), {
						items: this.path
					}, {
						params: {
							cmd: 'delete'
						}
					}).then(function(response) {
						notifications.success('Deleted ' + (self.isDir ? 'directory' : 'file') + "'" + self.name + "'");
					});
				};

				p.rename = function(newName) {
					var self = this;

					return $http.post(config.get('connectorUrl'), {
						path: this.path,
						name: newName
					}, {
						params: {
							cmd: 'rename'
						}
					}).then(function(response) {
						self.setData(response.data);
						notifications.success('Rename complete');
					});
				};

				p.resize = function(newWidth, newHeight, copy) {
					return $http.post(config.get('connectorUrl'), {
						path: this.path,
						width: newWidth,
						height: newHeight,
						copy: copy
					}, {
						params: {
							cmd: 'resize'
						}
					}).then(function(response) {
						notifications.success('Resize complete');
					});
				};

				// Unpack archive to selected path
				p.unpack = function(path) {
					if (!this.isArchive()) {
						return;
					}
					var self = this;

					return $http.post(config.get('connectorUrl'), {
						archive: this.path,
						path: path
					}, {
						params: {
							cmd: 'unpack'
						}
					}).then(function(response) {
						notifications.success("Unpacked archive '" + self.name + "' to '" + urlHelper.basename(path) + "'");
					});
				};

				p.select = function() {
					this._isSelected = true;
				};

				p.deselect = function() {
					this._isSelected = false;
				};

				p.isSelected = function() {
					return this._isSelected;
				};

				p.toggleSelection = function() {
					this._isSelected = !this._isSelected;
				};

				p.isImage = function() {
					return /^image\/(gif|jpeg|png)/.test(this.mime);
				};

				p.isText = function() {
					return /^(text\/|application\/xml$)/.test(this.mime);
				};

				p.isArchive = function() {
					return /^application\/(x-gzip|zip|x-bzip2?|x-tar)$/.test(this.mime);
				};

				p.viewUrl = function() {
					var self = this;
					return urlHelper.buildUrl(config.get('connectorUrl'), {
						cmd: 'view',
						path: self.path,
						_: self.mtime
					});
				};

				p.downloadUrl = function() {
					var self = this;
					return urlHelper.buildUrl(config.get('connectorUrl'), {
						cmd: 'download',
						path: self.path,
						_: self.mtime
					});
				};

				p.thumbnail = function(width, height, crop) {
					crop = crop || false;
					return urlHelper.buildUrl(config.get('connectorUrl'), {
						cmd: 'thumbnail',
						path: this.path,
						w: width,
						h: height,
						c: crop ? 1 : 0,
						_: this.mtime
					});
				};

				return File;
		}]);
})();