(function() {
	'use strict';

	angular.module('fileman')
		.factory('textHelper', function() {
			return {
				pluralize: function(count, words, join) {
					join = join || false;
					var word = count == 1 ? words[0] : words[1];
					if (join) {
						return count + ' ' + word;
					}
					return word;
				}
			};
		});
})();