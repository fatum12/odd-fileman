(function() {
	'use strict';

	angular.module('fileman')
		.factory('fileTypeIcon', function() {
			var mimeToIcon = {
					directory: 'fa-folder',
					'application/pdf': 'fa-file-pdf-o',
					'application/msword': 'fa-file-word-o',
					'application/vnd.openxmlformats-officedocument.wordprocessingml.document': 'fa-file-word-o',
					'application/vnd.ms-excel': 'fa-file-excel-o',
					'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet': 'fa-file-excel-o',
					'application/vnd.ms-powerpoint': 'fa-file-powerpoint-o',
					'application/vnd.openxmlformats-officedocument.presentationml.presentation': 'fa-file-powerpoint-o'
				},
				regExps = {
					'^image\/': 'fa-file-image-o',
					'^audio\/': 'fa-file-audio-o',
					'^video\/': 'fa-file-video-o'
				};

			return {
				get: function(file) {
					var result = 'fa-file-o',
						regExpStr,
						exp;

					if (mimeToIcon.hasOwnProperty(file.mime)) {
						result = mimeToIcon[file.mime];
					} else if (file.isText()) {
						result = 'fa-file-text-o';
					} else if (file.isArchive()) {
						result = 'fa-file-archive-o';
					} else {
						for (regExpStr in regExps) {
							exp = new RegExp(regExpStr, 'i');
							if (exp.test(file.mime)) {
								return regExps[regExpStr];
							}
						}
					}

					return result;
				}
			};
		});
})();