window.filemanManager = (function() {
	'use strict';

	var fm = {},
		configs = {};

	fm.widget = function(target, conf) {
		if (typeof conf !== 'object') {
			conf = {};
		}
		var config = extend({
				width: '100%',
				height: 600,
				selectFiles: false,
				onSelect: null
			}, conf),
			frame,
			id = target;

		checkConfig(config, ['templateUrl']);
		config.templateUrl += getQueryDelimiter(config.templateUrl) + 'filemanId=' + encodeURIComponent(id);
		saveConfig(id, config, ['connectorUrl', 'selectFiles', 'onSelect']);

		frame = document.createElement('iframe');
		frame.setAttribute('src', config.templateUrl);
		frame.setAttribute('scrolling', 'auto');
		frame.style.width = isNumeric(config.width) ? config.width + 'px' : config.width;
		frame.style.height = isNumeric(config.height) ? config.height + 'px' : config.height;
		frame.style.border = 'none';

		document.getElementById(target).appendChild(frame);
	};

	fm.window = function(conf) {
		if (typeof conf !== 'object') {
			conf = {};
		}
		var config = extend({
				width: 800,
				height: 600,
				selectFiles: false,
				onSelect: null
			}, conf),
			windowParams = '';

		checkConfig(config, ['templateUrl', 'id']);
		config.templateUrl += getQueryDelimiter(config.templateUrl) + 'filemanId=' + encodeURIComponent(config.id);
		saveConfig(config.id, config, ['connectorUrl', 'selectFiles', 'onSelect']);

		windowParams += 'width=' + config.width + ',height=' + config.height;
		windowParams += 'menubar=no,toolbar=no,location=yes,resizable=yes,scrollbars=yes';

		window.open(config.templateUrl, config.id, windowParams);
	};

	fm.getConfig = function(id) {
		if (configs.hasOwnProperty(id)) {
			return configs[id];
		}
		return null;
	};

	function extend(dest) {
		var objects = Array.prototype.slice.call(arguments, 1),
			i, j;
		for (i = 0; i < objects.length; i++) {
			var obj = objects[i];
			for (j in obj) {
				if (obj.hasOwnProperty(j)) {
					dest[j] = obj[j];
				}
			}
		}

		return dest;
	}

	function isNumeric(n) {
		return !isNaN(parseFloat(n)) && isFinite(n);
	}

	function checkConfig(config, requireKeys) {
		var i, ii;
		for (i = 0, ii = requireKeys.length; i < ii; i++) {
			if (!config.hasOwnProperty(requireKeys[i])) {
				throw new Error("Parameter '" + requireKeys[i] + "' is required");
			}
		}
	}

	function saveConfig(id, config, keys) {
		var newConfig = {},
			key,
			i,
			ii;

		for (i = 0, ii = keys.length; i < ii; i++) {
			key = keys[i];
			if (config.hasOwnProperty(key)) {
				newConfig[key] = config[key];
			}
		}
		configs[id] = newConfig;
	}

	function getQueryDelimiter(url) {
		return url.indexOf('?') != -1 ? '&' : '?';
	}

	return fm;
})();