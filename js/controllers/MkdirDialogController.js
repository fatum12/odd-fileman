(function() {
	'use strict';

	angular.module('fileman')
		.controller('MkdirDialogController', ['$uibModalInstance', function($uibModalInstance) {
			var dialog = this;

			dialog.name = '';

			dialog.ok = function () {
				$uibModalInstance.close(dialog.name);
			};

			dialog.cancel = function () {
				$uibModalInstance.dismiss('cancel');
			};
		}]);
})();