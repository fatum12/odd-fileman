(function() {
	'use strict';

	angular.module('fileman')
		.controller('RenameDialogController', ['$uibModalInstance', 'options', function($uibModalInstance, options) {
			var dialog = this;

			dialog.name = options.file.name;
			dialog.isDir = options.file.isDir;

			dialog.ok = function () {
				$uibModalInstance.close(dialog.name);
			};

			dialog.cancel = function () {
				$uibModalInstance.dismiss('cancel');
			};
		}]);
})();