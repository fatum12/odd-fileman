(function() {
	'use strict';

	angular.module('fileman')
		.controller('UploadDialogController', ['$uibModalInstance', 'Upload', 'config', 'options', 'filesManager', 'notifications',
			function($uibModalInstance, Upload, config, options, filesManager, notifications) {
				var dialog = this;

				dialog.files = [];
				dialog.maxSize = config.get('uploadMaxFileSize');

				dialog.upload = function(files, errFiles) {
					if (files && files.length) {
						dialog.files = dialog.files.concat(files);

						angular.forEach(files, function(file) {
							file.upload = Upload.upload({
								url: config.get('connectorUrl'),
								params: {
									cmd: 'upload'
								},
								data: {
									file: file,
									path: options.path
								}
							});

							file.upload.then(function (response) {
								filesManager.add(response.data);
							}, function (response) {
								file.$error = true;
							}, function (evt) {
								file.progress = Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
								if (file.progress >= 100) {
									file.done = true;
								}
							});
						});
					}
					// show errors
					angular.forEach(errFiles, function(errFile) {
						var errorText = "Failed to upload file '" + errFile.name + "'";
						switch (errFile.$error) {
							case 'maxSize':
								errorText = "File '" + errFile.name + "' is too big";
								break;
						}
						notifications.error(errorText);
					});
				};

				dialog.cancel = function () {
					$uibModalInstance.dismiss('cancel');
				};
		}]);
})();