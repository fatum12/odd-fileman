(function() {
	'use strict';

	angular.module('fileman')
		.controller('TextViewDialogController', ['$http', '$uibModalInstance', 'file', function($http, $uibModalInstance, file) {
			var dialog = this;

			dialog.file = file;
			dialog.content = '';
			dialog.isLoading = true;

			$http
				.get(file.viewUrl())
				.then(function(response) {
					dialog.content = response.data.content;
					dialog.isLoading = false;
				});

			dialog.cancel = function () {
				$uibModalInstance.dismiss('cancel');
			};

			dialog.fileLang = function() {
				if (file.isArchive()) {
					return 'plain';
				}
				return '';
			};
		}]);
})();