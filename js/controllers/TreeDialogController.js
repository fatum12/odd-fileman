(function() {
	'use strict';

	angular.module('fileman')
		.controller('TreeDialogController', ['$http', '$uibModalInstance', 'config', 'options',
			function($http, $uibModalInstance, config, options) {
				var dialog = this;

				dialog.title = options.title || '';
				dialog.tree = [];
				dialog.selected = null;

				dialog.options = {
					onExpand: function($event, node, context) {
						if (node.$model.hasOwnProperty('processed')) {
							return;
						}
						loadDirs(node.$model.path)
							.then(function(dirs) {
								if (dirs.length) {
									node.$model.children = dirs;
									node.$children = context.nodifyArray(dirs);
								} else {
									node.$model.has_children = false;
									node.$hasChildren = false;
									delete node.$model.children;
								}
								node.$model.processed = true;
							});
					},
					onSelect: function($event, node, context) {
						dialog.selected = node;
					}
				};

				dialog.context = {
					isSelected: function(node) {
						return dialog.selected === node;
					}
				};

				loadDirs('/')
					.then(function(dirs) {
						dialog.tree = dirs;
						dialog.tree.unshift({
							name: 'home',
							path: '/'
						});
					});

				dialog.ok = function () {
					if (!dialog.hasSelection()) {
						return;
					}
					$uibModalInstance.close(dialog.selected.$model.path);
				};

				dialog.cancel = function () {
					$uibModalInstance.dismiss('cancel');
				};

				dialog.hasSelection = function() {
					return dialog.selected !== null;
				};

				function loadDirs(path) {
					return $http.get(config.get('connectorUrl'), {
						params: {
							cmd: 'ls',
							path: path,
							only_dirs: true
						}
					}).then(function(response) {
						return response.data.map(function(dir) {
							return {
								name: dir.name,
								path: dir.path,
								children: []
							};
						});
					});
				}
		}]);
})();