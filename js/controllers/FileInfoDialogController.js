(function() {
	'use strict';

	angular.module('fileman')
		.controller('FileInfoDialogController', ['$uibModalInstance', 'fileTypeIcon', 'filesManager', 'options',
			function($uibModalInstance, fileTypeIcon, filesManager, options) {
				var dialog = this;

				dialog.file = options.file;

				dialog.iconClass = function() {
					return fileTypeIcon.get(options.file);
				};

				dialog.download = function() {
					filesManager.download(options.file);
				};

				dialog.cancel = function() {
					$uibModalInstance.dismiss('cancel');
				};
		}]);
})();