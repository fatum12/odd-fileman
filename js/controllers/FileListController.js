(function() {
	'use strict';

	angular.module('fileman')
		.controller('FileListController', ['$window', '$routeParams', 'filesManager', 'config', 'dialogs', 'textHelper', 'bytesFilter', 'fileTypeIcon', 'File',
			function($window, $routeParams, filesManager, config, dialogs, textHelper, bytesFilter, fileTypeIcon, File) {
				var list = this,
					lastSelected = null;

				// sorting types
				list.SORT_NAME = ['-isDir', 'name'];
				list.SORT_SIZE = ['-isDir', 'size'];
				list.SORT_DATE = ['-isDir', 'mtime'];
				list.SORT_TYPE = ['-isDir', 'mime'];

				list.path = $routeParams.path ? '/' + $routeParams.path : '/';
				list.files = filesManager.getFiles();
				list.currentDir = new File();
				list.currentDir.load(list.path);

				filesManager.ls(list.path);

				list.mkdir = function() {
					dialogs
						.mkdir()
						.then(function(directoryName) {
							filesManager.mkdir(list.path, directoryName);
						});
				};

				list.delete = function() {
					if (!filesManager.hasSelection()) {
						return;
					}
					var selectedFiles = filesManager.getSelection();
					var count = selectedFiles.length + ' item' + (selectedFiles.length > 1 ? 's' : '');
					dialogs
						.yesNo({
							title: 'Delete ' + count,
							message: 'Are you sure want to delete ' + count + '?'
						})
						.then(function() {
							filesManager
								.delete(selectedFiles.map(function(file) {
									return file.path;
								}))
								.then(function() {
									filesManager.ls(list.path);
								});
						});
				};

				list.rename = function() {
					if (!filesManager.hasSelection()) {
						return;
					}
					var file = filesManager.getSelection()[0];
					dialogs
						.rename({
							file: file
						})
						.then(function(newName) {
							file.rename(newName)
								.then(function() {
									filesManager.reorder();
								});
						});
				};

				list.upload = function() {
					dialogs.upload({
						path: list.path
					});
				};

				list.hasSelectedFiles = function() {
					return filesManager.hasSelection();
				};

				list.hasMultiselectedFiles = function() {
					return filesManager.hasMultiselection();
				};

				list.hasSingleFileSelected = function() {
					return filesManager.hasSingleFileSelected();
				};

				list.hasSingleImageSelected = function() {
					return filesManager.hasSingleImageSelected();
				};

				list.hasSingleArchiveSelected = function() {
					return filesManager.hasSingleArchiveSelected();
				};

				list.selectFile = function(event, file) {
					if (event.ctrlKey) {
						// multiselection
						file.toggleSelection();
					} else if (event.shiftKey) {
						// range selection
						if (!lastSelected) {
							lastSelected = list.files[0];
						}
						var start = list.files.indexOf(file),
							end = list.files.indexOf(lastSelected);

						angular.forEach(list.files.slice(Math.min(start, end), Math.max(start, end) + 1), function(f) {
							f.select();
						});
					} else {
						filesManager.deselectAll();
						file.select();
					}

					lastSelected = file;
				};

				list.preview = function(file) {
					if (file.isImage()) {
						var images = filesManager.getImages();
						dialogs.image({
							images: images,
							index: images.indexOf(file)
						});
					} else if (file.isText() || file.isArchive()) {
						dialogs.textView({
							file: file
						});
					} else {
						dialogs.fileInfo({
							file: file
						});
					}
				};

				list.copy = function() {
					var selectedFiles = filesManager.getSelection();
					dialogs
						.tree({
							title: 'Copy ' + selectedFiles.length + ' item' + (selectedFiles.length > 1 ? 's' : '') + ' to...'
						})
						.then(function(path) {
							filesManager
								.copy(selectedFiles.map(function(file) {
									return file.path;
								}), path)
								.then(function() {
									filesManager.ls(list.path);
								});
						});
				};

				list.move = function() {
					var selectedFiles = filesManager.getSelection();
					dialogs
						.tree({
							title: 'Move ' + selectedFiles.length + ' item' + (selectedFiles.length > 1 ? 's' : '') + ' to...'
						})
						.then(function(path) {
							filesManager
								.move(selectedFiles.map(function(file) {
									return file.path;
								}), path)
								.then(function() {
									filesManager.ls(list.path);
								});
						});
				};

				list.download = function() {
					if (!list.hasSingleFileSelected()) {
						return;
					}
					var selectedFiles = filesManager.getSelection();
					filesManager.download(selectedFiles[0]);
				};

				list.resize = function() {
					if (!list.hasSingleImageSelected()) {
						return;
					}

					var selectedFile = filesManager.getSelection()[0];
					dialogs
						.resize({
							file: selectedFile
						})
						.then(function(params) {
							selectedFile.resize(params.width, params.height, params.copy)
								.then(function() {
									filesManager.ls(list.path);
								});
						});
				};

				list.unpack = function() {
					if (!list.hasSingleArchiveSelected()) {
						return;
					}

					var selectedFile = filesManager.getSelection()[0];
					dialogs
						.tree({
							title: 'Unpack archive "' + selectedFile.name + '" to...'
						})
						.then(function(path) {
							selectedFile.unpack(path)
								.then(function() {
									if (path == list.path) {
										filesManager.ls(list.path);
									}
								});
						});
				};

				list.zip = function() {
					if (!list.hasSelectedFiles() || list.hasSingleArchiveSelected()) {
						return;
					}
					var selectedFiles = filesManager.getSelection();
					dialogs
						.zip()
						.then(function(archiveName) {
							filesManager.zip(selectedFiles.map(function(file) {
								return file.path
							}), list.path, archiveName);
						});
				};

				list.status = function() {
					var selectedFiles = filesManager.getSelection(),
						files = selectedFiles.length ? selectedFiles : list.files,
						filesCount = 0,
						dirsCount = 0,
						size = 0,
						result = '';

					angular.forEach(files, function(file) {
						if (file.isDir) {
							dirsCount++;
						} else {
							filesCount++;
							size += file.size;
						}
					});
					if (selectedFiles.length) {
						result += 'Selected ';
					}
					if (dirsCount > 0) {
						result += textHelper.pluralize(dirsCount, ['directory', 'directories'], true);
					}
					if (dirsCount > 0 && filesCount > 0) {
						result += ' and ';
					}
					if (filesCount > 0) {
						result += textHelper.pluralize(filesCount, ['file', 'files'], true);
					}
					if (size > 0) {
						result += ' (' + bytesFilter(size) + ')';
					}
					return result;
				};

				list.iconClass = function(file) {
					return fileTypeIcon.get(file);
				};

				list.isReadOnly = function() {
					return config.get('readOnly');
				};

				list.canChoose = function() {
					return config.get('selectFiles') && list.hasSingleFileSelected();
				};

				list.choose = function() {
					if (!list.canChoose()) {
						return;
					}
					var selectedFile = filesManager.getSelection()[0],
						callback = config.get('onSelect');
					if (angular.isFunction(callback)) {
						callback.call(null, selectedFile);
						// close window
						if ($window.opener) {
							$window.close();
						}
					}
				};
		}]);
})();