(function() {
	'use strict';

	angular.module('fileman')
		.controller('ResizeDialogController', ['$uibModalInstance', 'options', function($uibModalInstance, options) {
			var dialog = this;

			dialog.file = options.file;
			dialog.newImage = false;

			dialog.ok = function() {
				dialog.width = dialog.width || 0;
				dialog.height = dialog.height || 0;
				if (dialog.width == 0 && dialog.height == 0) {
					return;
				}

				$uibModalInstance.close({
					width: dialog.width,
					height: dialog.height,
					copy: dialog.newImage
				});
			};

			dialog.cancel = function() {
				$uibModalInstance.dismiss('cancel');
			};

			dialog.calcHeight = function() {
				var newWidth = dialog.width || 0;
				dialog.height = Math.round(dialog.file.height * newWidth / dialog.file.width);
			};

			dialog.calcWidth = function() {
				var newHeight = dialog.height || 0;
				dialog.width = Math.round(dialog.file.width * newHeight / dialog.file.height);
			};

			dialog.reset = function() {
				dialog.width = options.file.width;
				dialog.height = options.file.height;
			};

			dialog.reset();
		}]);
})();