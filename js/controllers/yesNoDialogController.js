(function() {
	'use strict';

	angular.module('fileman')
		.controller('YesNoDialogController', ['$uibModalInstance', 'options', function($uibModalInstance, options) {
			var dialog = this;

			dialog.title = options.title;
			dialog.message = options.message;

			dialog.yes = function () {
				$uibModalInstance.close();
			};

			dialog.no = function () {
				$uibModalInstance.dismiss();
			};
		}]);
})();