(function() {
	'use strict';

	angular.module('fileman')
		.directive('fmBreadcrumbs', function() {
			return {
				restrict: 'E',
				replace: true,
				templateUrl: 'templates/directives/breadcrumbs.html',
				scope: {
					path: '='
				},
				controller: ['$scope', function($scope) {
					$scope.breadcrumbs = [];

					var segments = $scope.path.split('/'),
						segmentIndex,
						subpath;

					segments.shift();

					$scope.breadcrumbs.push({
						path: '',
						name: 'home'
					});

					if ($scope.path != '/') {
						for (segmentIndex = 0; segmentIndex < segments.length; segmentIndex++) {
							subpath = segments.slice(0, segmentIndex + 1).join('/');
							$scope.breadcrumbs.push({
								path: '/' + subpath,
								name: segments[segmentIndex]
							});
						}
					}
				}]
			};
		});
})();