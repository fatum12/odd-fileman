(function() {
	'use strict';

	angular.module('fileman')
		.directive('fmHighlight', [function() {
			return {
				restrict: 'A',
				link: function(scope, element, attrs) {
					scope.$watch(attrs.fmHighlightSource, function(newValue, oldValue) {
						if (newValue) {
							element.html('<pre><code></code></pre>');
							var code = element.find('code');
							if (attrs.fmHighlightLang) {
								code.addClass(attrs.fmHighlightLang);
							}
							code.text(newValue);
							hljs.highlightBlock(code[0]);
						} else {
							element.empty();
						}
					});
				}
			};
		}]);
})();