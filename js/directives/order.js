(function() {
	'use strict';

	angular.module('fileman')
		.directive('fmOrder', function() {
			return {
				restrict: 'A',
				transclude: true,
				templateUrl: 'templates/directives/order.html',
				scope: {
					order: '='
				},
				controller: ['$scope', 'config', 'filesManager', function($scope, config, filesManager) {
					var reverse = angular.copy($scope.order);
					if (reverse[1].charAt(0) == '-') {
						reverse[1] = reverse[1].substr(1);
					} else {
						reverse[1] = '-' + reverse[1];
					}

					$scope.setOrder = function() {
						if ($scope.isOrder()) {
							config.set('order', reverse);
						} else {
							config.set('order', $scope.order);
						}
						filesManager.reorder();
					};

					$scope.isOrder = function() {
						return angular.equals(config.get('order'), $scope.order);
					};

					$scope.isReverseOrder = function() {
						return angular.equals(config.get('order'), reverse);
					};
				}]
			};
		});
})();