(function() {
	'use strict';

	angular.module('fileman')
		.directive('fmValidFilename', function() {
			return {
				require: 'ngModel',
				link: function(scope, elm, attrs, ctrl) {
					ctrl.$validators.fmValidFilename = function(modelValue, viewValue) {
						if (ctrl.$isEmpty(modelValue)) {
							// consider empty models to be valid
							return true;
						}

						if (/^[^/?*:;{}\\]+$/.test(modelValue)) {
							// it is valid
							return true;
						}

						// it is invalid
						return false;
					};
				}
			};
		});
})();