(function() {
	'use strict';

	angular.module('fileman')
		.directive('fmSelectFilename', ['$timeout', function($timeout) {
			return {
				restrict: 'A',
				scope: {
					value: '=ngModel',
					enabled: '=fmSelectFilename'
				},
				link: function(scope, element, attrs) {
					var dotPos = scope.enabled && scope.value.lastIndexOf('.') > -1 ? scope.value.lastIndexOf('.') : scope.value.length;
					if (dotPos > 0) {
						$timeout(function() {
							element[0].focus();
							element[0].setSelectionRange(0, dotPos);
						}, 100, false);
					}
				}
			};
		}]);
})();