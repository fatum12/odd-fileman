(function() {
	'use strict';

	angular.module('fileman')
		.config(['$routeProvider', function($routeProvider) {
			$routeProvider
				.when('/home', {
					controller: 'FileListController',
					controllerAs: 'list',
					templateUrl: 'templates/files.html'
				})
				.when('/home/:path*', {
					controller: 'FileListController',
					controllerAs: 'list',
					templateUrl: 'templates/files.html'
				})
				.otherwise('/home');
		}]);
})();