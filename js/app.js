(function() {
	'use strict';

	angular.module('fileman', ['ngRoute', 'ui.bootstrap', 'ngFileUpload', 'ui.bootstrap.showErrors', 'ya.treeview'])
		.config(['$httpProvider', function($httpProvider) {
			$httpProvider.interceptors.push('httpErrorHandler');
		}])
		.run(['config', 'Upload', function(config, Upload) {
			if (config.get('order') === null) {
				config.set('order', ['-isDir', 'name']);
			}

			var uploadOptions = {};
			if (config.get('uploadMaxFileSize') !== null) {
				uploadOptions.ngfMaxSize = config.get('uploadMaxFileSize');
			}
			if (config.get('imageAutoOrient')) {
				uploadOptions.ngfFixOrientation = true;
			}
			Upload.setDefaults(uploadOptions);
		}]);
})();