<?php
namespace Fatum12\Fileman\Command;

use Fatum12\Fileman\Exception\ArgumentException;
use Fatum12\Fileman\Exception\ForbiddenException;
use Fatum12\Fileman\Exception\MethodNotAllowedException;
use Fatum12\Fileman\Http\Request;
use Fatum12\Fileman\Config;

abstract class AbstractCommand
{
	final public function __construct() {}

	public function execute(Request $request, Config $config)
	{
		$filters = $this->filters();
		// validate request type
		if (isset($filters['methods']) && !in_array($request->getMethod(), $filters['methods'])) {
			throw new MethodNotAllowedException('Method not allowed', 405, ['Allow' => implode(', ', $filters['methods'])]);
		}
		if ($config->get('readOnly') && isset($filters['disableOnReadOnly']) && $filters['disableOnReadOnly']) {
			throw new ForbiddenException('Command was disabled on server');
		}
		// validate request parameters
		if (isset($filters['required']) && is_array($filters['required'])) {
			$requestMethodName = $request->getMethod() == Request::METHOD_GET ? 'get' : 'post';
			foreach ($filters['required'] as $paramName) {
				$paramValue = $request->$requestMethodName($paramName);
				if (is_null($paramValue) || $paramValue === '') {
					throw new ArgumentException("Parameter '{$paramName}' is required");
				}
			}
		}

		$this->process($request, $config);
	}

	abstract protected function process(Request $request, Config $config);

	protected function filters()
	{
		return [];
	}
}