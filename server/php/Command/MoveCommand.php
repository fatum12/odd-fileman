<?php
namespace Fatum12\Fileman\Command;

use Fatum12\Fileman\Exception\ForbiddenException;
use Fatum12\Fileman\File\AbstractFile;
use Fatum12\Fileman\File\Directory;
use Fatum12\Fileman\Http\Request;
use Fatum12\Fileman\Http\JsonResponse;
use Fatum12\Fileman\Config;


class MoveCommand extends AbstractCommand
{
	protected function process(Request $request, Config $config)
	{
		$target = new Directory($request->post('target'), $config->get('root'));
		if (!$target->isWritable()) {
			throw new ForbiddenException("Directory '" . $target->getName() . "' not writable");
		}
		$items = $request->post('items', []);
		foreach ($items as $itemPath) {
			$item = AbstractFile::getInstance($itemPath, $config->get('root'));
			$item->moveTo($target);
		}

		(new JsonResponse(['status' => 'ok']))->send();
	}

	protected function filters()
	{
		return [
			'methods' => [Request::METHOD_POST],
			'disableOnReadOnly' => true,
			'required' => ['target', 'items']
		];
	}
}