<?php
namespace Fatum12\Fileman\Command;

use Fatum12\Fileman\File\File;
use Fatum12\Fileman\Http\JsonResponse;
use Fatum12\Fileman\Http\Request;
use Fatum12\Fileman\Config;


class ViewCommand extends AbstractCommand
{
	protected function process(Request $request, Config $config)
	{
		$file = new File($request->get('path'), $config->get('root'));
		if ($file->isImage()) {
			$file->output();
			exit;
		} elseif ($file->isText()) {
			(new JsonResponse([
				'content' => $file->getContent()
			]))->send();
		} elseif ($file->isArchive()) {
			$unicodeTreePrefix = function(\RecursiveTreeIterator $tree) {
				$prefixParts = [
					\RecursiveTreeIterator::PREFIX_LEFT         => ' ',
					\RecursiveTreeIterator::PREFIX_MID_HAS_NEXT => '│ ',
					\RecursiveTreeIterator::PREFIX_END_HAS_NEXT => '├ ',
					\RecursiveTreeIterator::PREFIX_END_LAST     => '└ '
				];
				foreach ($prefixParts as $part => $string) {
					$tree->setPrefixPart($part, $string);
				}
			};
			$dir  = new \RecursiveDirectoryIterator('phar://' . $file->getPath(), \RecursiveDirectoryIterator::KEY_AS_FILENAME | \RecursiveDirectoryIterator::SKIP_DOTS);
			$tree = new \RecursiveTreeIterator($dir, \RecursiveTreeIterator::BYPASS_KEY);
			$unicodeTreePrefix($tree);

			$content = '';
			foreach ($tree as $filename => $line) {
				$content .= $tree->getPrefix() . $filename . "\n";
			}

			(new JsonResponse([
				'content' => $content
			]))->send();
		}
	}

	protected function filters()
	{
		return [
			'methods' => [Request::METHOD_GET],
			'required' => ['path']
		];
	}
}