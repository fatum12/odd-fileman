<?php
namespace Fatum12\Fileman\Command;

use Fatum12\Fileman\Exception\ArgumentException;
use Fatum12\Fileman\File\AbstractFile;
use Fatum12\Fileman\File\Directory;
use Fatum12\Fileman\File\File;
use Fatum12\Fileman\Http\Request;
use Fatum12\Fileman\Http\JsonResponse;
use Fatum12\Fileman\Config;
use Fatum12\Fileman\Util\Path;


class ZipCommand extends AbstractCommand
{
	protected function process(Request $request, Config $config)
	{
		$path = new Directory($request->post('path'), $config->get('root'));
		if (!$path->isWritable()) {
			throw new ForbiddenException("Directory '" . $path->getName() . "' not writable");
		}
		$items = $request->post('items', []);
		// create zip archive
		$zip = new \ZipArchive();
		$archiveName = $request->post('archiveName');
		if (!preg_match('/\.zip$/', $archiveName)) {
			$archiveName .= '.zip';
		}
		if (!Path::isValidFileName($archiveName)) {
			throw new ArgumentException("'" . $archiveName . "' is not a valid file name");
		}
		$archiveName = Path::getUniqueFileName($path->getPath(), $archiveName);
		$zip->open($path->getPath() . '/' . $archiveName, \ZipArchive::CREATE);
		foreach ($items as $item) {
			$itemObj = AbstractFile::getInstance($item, $config->get('root'));
			if ($itemObj->isDir()) {
				$zip->addEmptyDir(str_replace($path->getRelativePath() . '/', '', $itemObj->getRelativePath()));
				$files = new \RecursiveIteratorIterator(
					new \RecursiveDirectoryIterator($itemObj->getPath(), \RecursiveDirectoryIterator::SKIP_DOTS),
					\RecursiveIteratorIterator::LEAVES_ONLY
				);

				foreach ($files as $file) {
					// get real and relative path for current file
					$filePath = $file->getRealPath();
					$relativePath = Path::normalize(substr($filePath, strlen($itemObj->getParentDir()) + 1));

					if ($file->isDir()) {
						$zip->addEmptyDir($relativePath);
					} else {
						$zip->addFile($filePath, $relativePath);
					}
				}
			} else {
				$zip->addFile($itemObj->getPath(), str_replace($path->getRelativePath() . '/', '', $itemObj->getRelativePath()));
			}
		}
		$zip->close();

		$archive = new File($path->getRelativePath() . '/' . $archiveName, $config->get('root'));

		(new JsonResponse($archive))->send();
	}

	protected function filters()
	{
		return [
			'methods' => [Request::METHOD_POST],
			'disableOnReadOnly' => true,
			'required' => ['path', 'archiveName']
		];
	}
}