<?php
namespace Fatum12\Fileman\Command;

use Fatum12\Fileman\File\Directory;
use Fatum12\Fileman\Http\Request;
use Fatum12\Fileman\Http\JsonResponse;
use Fatum12\Fileman\Config;


class MkdirCommand extends AbstractCommand
{
	protected function process(Request $request, Config $config)
	{
		$directory = new Directory($request->post('path'), $config->get('root'));
		$newDirectory = $directory->mkdir($request->post('name'), $config->get('mkdirMode'));
		(new JsonResponse($newDirectory))->send();
	}

	protected function filters()
	{
		return [
			'methods' => [Request::METHOD_POST],
			'disableOnReadOnly' => true,
			'required' => ['path', 'name']
		];
	}
}