<?php
namespace Fatum12\Fileman\Command;

use Fatum12\Fileman\Exception\ForbiddenException;
use Fatum12\Fileman\File\Directory;
use Fatum12\Fileman\File\File;
use Fatum12\Fileman\Http\JsonResponse;
use Fatum12\Fileman\Http\Request;
use Fatum12\Fileman\Config;

class UnpackCommand extends AbstractCommand
{
	protected function process(Request $request, Config $config)
	{
		$archive = new File($request->post('archive'), $config->get('root'));
		$dest = new Directory($request->post('path'), $config->get('root'));
		if (!$dest->isWritable()) {
			throw new ForbiddenException("Directory '" . $dest->getName() . "' not writable");
		}
		$archive->unpack($dest->getRelativePath());

		(new JsonResponse(['status' => 'ok']))->send();
	}

	protected function filters()
	{
		return [
			'methods' => [Request::METHOD_POST],
			'disableOnReadOnly' => true,
			'required' => ['archive', 'path']
		];
	}
}