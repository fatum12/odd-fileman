<?php
namespace Fatum12\Fileman\Command;

use Fatum12\Fileman\File\AbstractFile;
use Fatum12\Fileman\Http\Request;
use Fatum12\Fileman\Http\JsonResponse;
use Fatum12\Fileman\Config;


class RenameCommand extends AbstractCommand
{
	protected function process(Request $request, Config $config)
	{
		$target = AbstractFile::getInstance($request->post('path'), $config->get('root'));
		$newName = $request->post('name');
		$target->rename($newName);

		(new JsonResponse($target))->send();
	}

	protected function filters()
	{
		return [
			'methods' => [Request::METHOD_POST],
			'disableOnReadOnly' => true,
			'required' => ['path', 'name']
		];
	}
}