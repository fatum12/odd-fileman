<?php
namespace Fatum12\Fileman\Command;


use Fatum12\Fileman\File\File;
use Fatum12\Fileman\Http\Request;
use Fatum12\Fileman\Http\JsonResponse;
use Fatum12\Fileman\Config;

class ResizeCommand extends AbstractCommand
{
	protected function process(Request $request, Config $config)
	{
		$image = new File($request->post('path'), $config->get('root'));
		if ($request->post('copy', false)) {
			$copy = $image->duplicate();
			$copy->resize($request->post('width'), $request->post('height'));
		} else {
			$image->resize($request->post('width'), $request->post('height'));
		}

		(new JsonResponse(['status' => 'ok']))->send();
	}

	protected function filters()
	{
		return [
			'methods' => [Request::METHOD_POST],
			'disableOnReadOnly' => true,
			'required' => ['path', 'width', 'height']
		];
	}
}