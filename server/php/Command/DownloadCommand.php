<?php
namespace Fatum12\Fileman\Command;

use Fatum12\Fileman\File\File;
use Fatum12\Fileman\Http\Request;
use Fatum12\Fileman\Config;


class DownloadCommand extends AbstractCommand
{
	protected function process(Request $request, Config $config)
	{
		$file = new File($request->get('path'), $config->get('root'));
		$file->output(true);
		exit;
	}

	protected function filters()
	{
		return [
			'methods' => [Request::METHOD_GET],
			'required' => ['path']
		];
	}
}