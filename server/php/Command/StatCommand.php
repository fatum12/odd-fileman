<?php
namespace Fatum12\Fileman\Command;

use Fatum12\Fileman\File\AbstractFile;
use Fatum12\Fileman\Http\Request;
use Fatum12\Fileman\Http\JsonResponse;
use Fatum12\Fileman\Config;


class StatCommand extends AbstractCommand
{
	protected function process(Request $request, Config $config)
	{
		$item = AbstractFile::getInstance($request->get('path'), $config->get('root'));
		(new JsonResponse($item))->send();
	}

	protected function filters()
	{
		return [
			'methods' => [Request::METHOD_GET],
			'required' => ['path']
		];
	}
}