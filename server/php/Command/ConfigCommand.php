<?php
namespace Fatum12\Fileman\Command;

use Fatum12\Fileman\Http\Request;
use Fatum12\Fileman\Http\JsonResponse;
use Fatum12\Fileman\Config;


class ConfigCommand extends AbstractCommand
{
	protected function process(Request $request, Config $config)
	{
		$options = $config->get();
		unset($options['root']);
		(new JsonResponse($options))->send();
	}

	protected function filters()
	{
		return [
			'methods' => [Request::METHOD_GET]
		];
	}
}