<?php
namespace Fatum12\Fileman\Command;

use Fatum12\Fileman\File\Directory;
use Fatum12\Fileman\Http\Request;
use Fatum12\Fileman\Http\JsonResponse;
use Fatum12\Fileman\Config;


class LsCommand extends AbstractCommand
{
	protected function process(Request $request, Config $config)
	{
		$directory = new Directory($request->get('path'), $config->get('root'));
		(new JsonResponse($directory->ls($request->get('only_dirs', false))))->send();
	}

	protected function filters()
	{
		return [
			'methods' => [Request::METHOD_GET],
			'required' => ['path']
		];
	}
}