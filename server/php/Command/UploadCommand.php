<?php
namespace Fatum12\Fileman\Command;

use Fatum12\Fileman\Exception\ArgumentException;
use Fatum12\Fileman\Exception\BaseException;
use Fatum12\Fileman\Exception\ForbiddenException;
use Fatum12\Fileman\File\Directory;
use Fatum12\Fileman\File\File;
use Fatum12\Fileman\Http\Request;
use Fatum12\Fileman\Http\JsonResponse;
use Fatum12\Fileman\Config;
use Fatum12\Fileman\Util\Path;


class UploadCommand extends AbstractCommand
{
	protected function process(Request $request, Config $config)
	{
		$targetDir = new Directory($request->post('path'), $config->get('root'));
		if (!$targetDir->isWritable()) {
			throw new ForbiddenException("Directory '" . $targetDir->getName() + "' not writable");
		}
		$tmpFile = $request->file('file');
		if (is_null($tmpFile)) {
			throw new ArgumentException('File is required');
		}
		// check file type
		if ($config->get('uploadFileTypes')) {
			if (!preg_match($config->get('uploadFileTypes'), $tmpFile['name'])) {
				throw new BaseException('Filetype not allowed');
			}
		}
		// check file size
		if (!is_null($config->get('uploadMaxFileSize')) && $tmpFile['size'] > $config->get('uploadMaxFileSize')) {
			throw new BaseException("File '" . $tmpFile['name'] . "' is too big");
		}
		$uploadFileName = Path::getUniqueFileName($targetDir->getPath(), basename($tmpFile['name']));
		if (move_uploaded_file($tmpFile['tmp_name'], $targetDir->getPath() . '/' . $uploadFileName)) {
			$uploadedFile = new File($targetDir->getRelativePath() . '/' . $uploadFileName, $config->get('root'));
			if ($uploadedFile->isImage()) {
				if ($config->get('imageMaxWidth') || $config->get('imageMaxHeight')) {
					$uploadedFile->resize($config->get('imageMaxWidth'), $config->get('imageMaxHeight'));
				}
				if ($config->get('imageAutoOrient')) {
					$uploadedFile
						->getImageObject()
						->orientate()
						->save();
				}
			}
			(new JsonResponse($uploadedFile))->send();
		} else {
			throw new BaseException('Upload failed');
		}
	}

	protected function filters()
	{
		return [
			'methods' => [Request::METHOD_POST],
			'disableOnReadOnly' => true,
			'required' => ['path']
		];
	}
}