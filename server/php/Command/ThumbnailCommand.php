<?php
namespace Fatum12\Fileman\Command;

use Fatum12\Fileman\File\File;
use Fatum12\Fileman\Http\Request;
use Fatum12\Fileman\Config;
use Fatum12\Fileman\Http\Response;


class ThumbnailCommand extends AbstractCommand
{
	protected function process(Request $request, Config $config)
	{
		$file = new File($request->get('path'), $config->get('root'));
		$imageObj = $file->getImageObject();
		if ($request->get('c', false)) {
			$imageObj->fit($request->get('w'), $request->get('h'));
		} else {
			$imageObj->resize($request->get('w'), $request->get('h'), function($constraint) {
				$constraint->aspectRatio();
			});
		}
		// browser cache headers
		$cacheTime = 10 * 24 * 3600;	// 10 days
		$response = new Response('', 200, [
			'Pragma' => 'public',
			'Cache-Control' => 'max-age=' . $cacheTime,
			'Expires' => gmdate('D, d M Y H:i:s \G\M\T', time() + $cacheTime),
			'Content-Type' => $file->getMimeType()
		]);
		$response->send();
		echo $imageObj->encode();
		exit;
	}

	protected function filters()
	{
		return [
			'methods' => [Request::METHOD_GET],
			'required' => ['path', 'w', 'h']
		];
	}
}