<?php
namespace Fatum12\Fileman\Command;

use Fatum12\Fileman\File\AbstractFile;
use Fatum12\Fileman\Http\Request;
use Fatum12\Fileman\Http\JsonResponse;
use Fatum12\Fileman\Config;


class DeleteCommand extends AbstractCommand
{
	protected function process(Request $request, Config $config)
	{
		$items = $request->post('items');
		if (!is_array($items)) {
			$items = [$items];
		}
		foreach ($items as $item) {
			$file = AbstractFile::getInstance($item, $config->get('root'));
			$file->delete();
		}

		(new JsonResponse(['status' => 'ok']))->send();
	}

	protected function filters()
	{
		return [
			'methods' => [Request::METHOD_POST],
			'disableOnReadOnly' => true,
			'required' => ['items']
		];
	}
}