<?php
namespace Fatum12\Fileman;

use Fatum12\Fileman\Exception\BaseException;
use Fatum12\Fileman\Exception\HttpException;
use Fatum12\Fileman\Exception\NotFoundException;
use Fatum12\Fileman\Http\Request;
use Fatum12\Fileman\Http\Response;


class Fileman
{
	protected $root;
	/**
	 * @var Config
	 */
	protected $config;

	public function __construct($root, array $options = [])
	{
		$this->root = realpath($root);

		$options = array_replace([
			'uploadMaxFileSize' => null,
			// defines which files (based on their names) are accepted for upload
			'uploadFileTypes' => '/.+$/i',
			// image resolution restrictions
			'imageMaxWidth' => null,
			'imageMaxHeight' => null,
			// automatically rotate images based on EXIF meta data
			'imageAutoOrient' => true,
			// restrict all write commands
			'readOnly' => false,
			'mkdirMode' => 0755,
		], $options);
		$this->config = new Config($options);
		$this->config->set('root', $this->root);
	}

	public function handleRequest()
	{
		try {
			$request = new Request();
			$commandName = $request->get('cmd');
			$commandClass = __NAMESPACE__ . '\\Command\\' . ucfirst($commandName) . 'Command';
			if (class_exists($commandClass)) {
				$command = new $commandClass();
				$command->execute($request, $this->config);
			}
			else {
				throw new NotFoundException("Command '{$commandName}' not found");
			}
		} catch (HttpException $e) {
			(new Response($e->getMessage(), $e->getCode(), $e->getHeaders()))->send();
			return;
		} catch (BaseException $e) {
			(new Response($e->getMessage(), 400))->send();
		}
	}
}