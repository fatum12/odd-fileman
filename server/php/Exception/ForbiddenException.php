<?php
namespace Fatum12\Fileman\Exception;


use Exception;

class ForbiddenException extends HttpException
{

	public function __construct($message = 'Forbidden', $code = 403, array $headers = [], Exception $previous = null)
	{
		parent::__construct($message, $code, $headers, $previous);
	}
}