<?php
namespace Fatum12\Fileman\Exception;


use Exception;

class MethodNotAllowedException extends HttpException
{
	public function __construct($message = 'Method Not Allowed', $code = 405, array $headers = [], Exception $previous = null)
	{
		parent::__construct($message, $code, $headers, $previous);
	}
}