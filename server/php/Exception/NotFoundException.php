<?php
namespace Fatum12\Fileman\Exception;


use Exception;

class NotFoundException extends HttpException
{

	public function __construct($message = 'Not Found', $code = 404, array $headers = [], Exception $previous = null)
	{
		parent::__construct($message, $code, $headers, $previous);
	}
}