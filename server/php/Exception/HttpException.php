<?php
namespace Fatum12\Fileman\Exception;


use Exception;

class HttpException extends BaseException
{
	protected $headers = [];

	public function __construct($message = '', $code = 400, array $headers = [], Exception $previous = null)
	{
		parent::__construct($message, $code, $previous);
		$this->headers = $headers;
	}

	public function getHeaders()
	{
		return $this->headers;
	}
}