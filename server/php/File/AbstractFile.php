<?php
namespace Fatum12\Fileman\File;

use Fatum12\Fileman\Exception\BaseException;
use Fatum12\Fileman\Exception\ForbiddenException;
use Fatum12\Fileman\Util\Path;


abstract class AbstractFile implements \JsonSerializable
{
	protected $relativePath;
	protected $path;
	protected $root;
	/**
	 * @var \SplFileInfo
	 */
	protected $fileInfo;

	private $mimeType;

	public function __construct($relativePath, $root)
	{
		$this->root = $root;

		$this->update($relativePath);
	}

	public static function getInstance($relativePath, $root)
	{
		if (is_dir($root . $relativePath)) {
			return new Directory($relativePath, $root);
		} else {
			return new File($relativePath, $root);
		}
	}

	public function jsonSerialize()
	{
		return [
			'path' => $this->getRelativePath(),
			'name' => $this->getName(),
			'size' => $this->getSize(),
			'isDir' => false,
			'mtime' => $this->getMTime(),
			'mime' => $this->getMimeType(),
			'writable' => $this->isWritable()
		];
	}

	public function getMimeType()
	{
		if (!$this->mimeType) {
			$finfo = finfo_open(FILEINFO_MIME_TYPE);
			$this->mimeType = finfo_file($finfo, $this->getPath());
			finfo_close($finfo);
		}
		return $this->mimeType;
	}

	public function getSize()
	{
		return $this->fileInfo->getSize();
	}

	public function getMTime()
	{
		return $this->fileInfo->getMTime();
	}

	public function isWritable()
	{
		return $this->fileInfo->isWritable();
	}

	public function delete()
	{
		$this->assertNotRoot();
		if (!@unlink($this->getPath())) {
			throw new BaseException("Can't delete '" . $this->getName() . "'");
		}
	}

	public function rename($newName)
	{
		$this->assertNotRoot();
		$newPath = $this->getParentDir() . '/' . $newName;
		if (@rename($this->getPath(), $newPath)) {
			$this->update($this->getRelativeParentDir() . '/' . $newName);
		} else {
			throw new BaseException("Can't rename '" . $this->getName() . "'");
		}
	}

	public function copyTo(Directory $target)
	{
		$uniqueName = Path::getUniqueFileName($target->getPath(), $this->getName());
		if (!copy($this->getPath(), $target->getPath() . '/' . $uniqueName)) {
			throw new BaseException("Can't copy '" . $this->getName() . "'");
		}
		return $target->getRelativePath() . '/' . $uniqueName;
	}

	public function moveTo(Directory $target)
	{
		$this->assertNotRoot();
		if (@rename($this->getPath(), $target->getPath() . '/' . $this->getName())) {
			$this->update($target->getRelativePath() . '/' . $this->getName());
		} else {
			throw new BaseException("Can't move '" . $this->getName() . "'");
		}
	}

	public function getPath()
	{
		return $this->path;
	}

	public function getRelativePath()
	{
		return $this->relativePath;
	}

	public function getName()
	{
		return $this->fileInfo->getBasename();
	}

	public function getParentDir()
	{
		return dirname($this->getPath());
	}

	public function getRelativeParentDir()
	{
		return dirname($this->getRelativePath());
	}

	public function isDir()
	{
		return false;
	}

	protected function update($relativePath)
	{
		$this->relativePath = Path::safe($relativePath);
		$this->path = realpath($this->root . $this->relativePath);
		$this->fileInfo = new \SplFileInfo($this->path);
	}

	protected function assertNotRoot()
	{
		if ($this->getRelativePath() == '/') {
			throw new ForbiddenException();
		}
	}
}