<?php
namespace Fatum12\Fileman\File;


use Fatum12\Fileman\Exception\ArgumentException;
use Fatum12\Fileman\Exception\BaseException;
use Fatum12\Fileman\Exception\ForbiddenException;
use Fatum12\Fileman\Exception\NotFoundException;

class Directory extends AbstractFile
{
	public function __construct($relativePath, $root)
	{
		parent::__construct($relativePath, $root);
		if (!is_dir($this->getPath())) {
			throw new NotFoundException("Directory '" . $this->getRelativePath() . "' not found");
		}
	}

	public function jsonSerialize()
	{
		$json = parent::jsonSerialize();
		$json['isDir'] = true;
		$json['size'] = 0;

		return $json;
	}

	public function ls($onlyDirs = false)
	{
		$files = scandir($this->getPath());
		$result = [];
		if ($files !== false) {
			foreach ($files as $file) {
				if ($file == '.' || $file == '..') {
					continue;
				}
				if ($onlyDirs) {
					if (is_dir($this->getPath() . '/' . $file)) {
						$result[] = new Directory($this->getRelativePath() . '/' . $file, $this->root);
					}
				} else {
					$result[] = AbstractFile::getInstance($this->getRelativePath() . '/' . $file, $this->root);
				}
			}
		}
		return $result;
	}

	public function mkdir($directoryName, $mode = 0755)
	{
		if (!$this->isWritable()) {
			throw new ForbiddenException("Directory '" . $this->getName() . "' not writable'");
		}
		if (!is_int($mode)) {
			throw new ArgumentException('Wrong mkdir mode');
		}
		if (!mkdir($this->getPath() . '/' . $directoryName, $mode)) {
			throw new BaseException("Can't make directory '{$directoryName}'");
		}
		return new Directory($this->getRelativePath() . '/' . $directoryName, $this->root);
	}

	public function delete()
	{
		$this->assertNotRoot();
		$this->doDelete($this->getPath());
	}

	public function copyTo(Directory $target)
	{
		$this->doCopy($this->getPath(), $target->getPath() . '/' . $this->getName());
	}

	public function isDir()
	{
		return true;
	}

	private function doDelete($dir)
	{
		if ($handle = opendir($dir)) {
			while (false !== ($entry = readdir($handle))) {
				if ($entry != '.' && $entry != '..') {
					if (is_dir($dir . '/' . $entry)) {
						$this->doDelete($dir . '/' . $entry);
					} else {
						if (!@unlink($dir . '/' . $entry)) {
							throw new BaseException("Can't delete '" . $this->getName() . "'");
						}
					}
				}
			}
			closedir($handle);
		}
		if (!@rmdir($dir)) {
			throw new BaseException("Can't delete '" . $this->getName() . "'");
		}
	}

	private function doCopy($src, $dst)
	{
		$dir = opendir($src);
		@mkdir($dst);
		while (false !== ($file = readdir($dir))) {
			if ($file != '.' && $file != '..') {
				if (is_dir($src . '/' . $file)) {
					$this->doCopy($src . '/' . $file, $dst . '/' . $file);
				} else {
					copy($src . '/' . $file, $dst . '/' . $file);
				}
			}
		}
		closedir($dir);
	}
}