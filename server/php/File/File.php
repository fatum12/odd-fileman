<?php
namespace Fatum12\Fileman\File;


use Intervention\Image\ImageManagerStatic as ImageManager;
use Fatum12\Fileman\Exception\ArgumentException;
use Fatum12\Fileman\Exception\BaseException;
use Fatum12\Fileman\Exception\NotFoundException;
use Fatum12\Fileman\Http\Response;

class File extends AbstractFile
{
	protected $imageSize;
	/**
	 * @var \Intervention\Image\Image
	 */
	protected $imageObject;

	/**
	 * File constructor.
	 */
	public function __construct($relativePath, $root)
	{
		parent::__construct($relativePath, $root);
		if (!is_file($this->getPath())) {
			throw new NotFoundException("File '" . $this->getRelativePath() . "' not found");
		}
	}

	public function jsonSerialize()
	{
		$json = parent::jsonSerialize();
		$json += $this->getImageSize();

		return $json;
	}

	/**
	 * Return file name without extension
	 */
	public function getNameWithoutExt()
	{
		return pathinfo($this->getPath(), PATHINFO_FILENAME);
	}

	public function isImage()
	{
		return preg_match('/^image\/(gif|jpeg|png)/', $this->getMimeType());
	}

	public function isText()
	{
		return preg_match('~^(text/|application/xml$)~', $this->getMimeType());
	}

	public function isArchive()
	{
		return preg_match('~^application/(x-gzip|zip|x-bzip2?|x-tar)$~', $this->getMimeType());
	}

	public function getContent()
	{
		return file_get_contents($this->getPath());
	}

	public function getImageSize()
	{
		if (!$this->isImage()) {
			return [
				'width' => 0,
				'height' => 0
			];
		}
		if (!$this->imageSize) {
			$imageInfo = getimagesize($this->getPath());
			$this->imageSize = [
				'width' => $imageInfo[0],
				'height' => $imageInfo[1]
			];
		}
		return $this->imageSize;
	}

	public function resize($width, $height)
	{
		if (!$this->isImage()) {
			throw new BaseException("File '" . $this->getName() . "' is not an image");
		}
		if (!is_null($width)) {
			$width = intval($width);
		}
		if (!is_null($height)) {
			$height = intval($height);
		}
		if (!$width && !$height) {
			throw new ArgumentException('Width or height is required');
		}
		$this->getImageObject()
			->resize($width, $height, function($constraint) {
				$constraint->aspectRatio();
				$constraint->upsize();
			})
			->save();
	}

	public function output($download = false)
	{
		if ($download) {
			$response = new Response('', 200, [
				'Content-Description' => 'File Transfer',
				'Content-Type' => $this->getMimeType(),
				'Content-Disposition' => 'attachment; filename="' . $this->getName() . '"',
				'Expires' => 0,
				'Cache-Control' => 'must-revalidate',
				'Pragma' => 'public',
				'Content-Length' => $this->getSize()
			]);
		} else {
			$cacheTime = 10 * 24 * 3600;	// 10 days
			$response = new Response('', 200, [
				'Pragma' => 'public',
				'Cache-Control' => 'max-age=' . $cacheTime,
				'Expires' => gmdate('D, d M Y H:i:s \G\M\T', time() + $cacheTime),
				'Content-Type' => $this->getMimeType(),
				'Last-Modified' => $this->getMTime()
			]);
		}

		$response->send();
		readfile($this->getPath());
	}

	public function duplicate()
	{
		$copyPath = $this->copyTo(new Directory(dirname($this->getRelativePath()), $this->root));
		return new File($copyPath, $this->root);
	}

	/**
	 * Extract archive to selected path
	 *
	 * @param string $path Relative path to destination directory
	 * @param bool $overwrite Overwrite existing files
	 * @throws BaseException if file is not an archive
	 */
	public function unpack($path, $overwrite = true)
	{
		if (!$this->isArchive()) {
			throw new BaseException("File '" . $this->getName() . "' is not an archive");
		}
		if (preg_match('/\.zip$/', $this->getName())) {
			$archive = new \ZipArchive();
			$archive->open($this->getPath());
			$archive->extractTo($this->root . $path);
			$archive->close();
		} else {
			$archive = new \PharData($this->getPath());
			$archive->extractTo($this->root . $path, null, $overwrite);
		}
	}

	public function getImageObject()
	{
		if (!$this->imageObject) {
			$this->imageObject = ImageManager::make($this->getPath());
		}
		return $this->imageObject;
	}
}