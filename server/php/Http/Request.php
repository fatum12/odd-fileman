<?php
namespace Fatum12\Fileman\Http;


class Request
{
	const METHOD_HEAD = 'HEAD';
	const METHOD_GET = 'GET';
	const METHOD_POST = 'POST';
	const METHOD_PUT = 'PUT';
	const METHOD_PATCH = 'PATCH';
	const METHOD_DELETE = 'DELETE';
	const METHOD_OPTIONS = 'OPTIONS';

	protected $post;

	public function __construct()
	{
		$this->post = isset($_POST) ? $_POST : [];

		if ($this->getMethod() == self::METHOD_POST && isset($_SERVER["CONTENT_TYPE"]) &&
			strpos($_SERVER['CONTENT_TYPE'], 'application/json') !== false) {
			$postdata = file_get_contents("php://input");
			$post = json_decode($postdata, true);
			if ($post !== null) {
				$this->post = array_merge($this->post, $post);
			}
		}
	}

	public function getMethod()
	{
		return $_SERVER['REQUEST_METHOD'];
	}

	public function get($key, $default = null)
	{
		if (isset($_GET[$key])) {
			return $_GET[$key];
		}
		return $default;
	}

	public function post($key, $default = null)
	{
		if (isset($this->post[$key])) {
			return $this->post[$key];
		}
		return $default;
	}

	public function file($key)
	{
		if (isset($_FILES[$key])) {
			return $_FILES[$key];
		}
		return null;
	}
}