<?php
namespace Fatum12\Fileman\Http;


class JsonResponse extends Response
{
	public function __construct($body = [], $status = 200, array $headers = [])
	{
		parent::__construct(json_encode($body), $status, $headers);
		$this->headers = array_replace([
			'Pragma' => 'no-cache',
			'Cache-Control' => 'no-store, no-cache, must-revalidate'
		], $this->headers);
		$this->headers['Content-Type'] = 'application/json';
	}
}