<?php
namespace Fatum12\Fileman\Http;


class Response
{
	protected $statusCode;
	protected $body;
	protected $headers = [];

	public function __construct($body = '', $status = 200, array $headers = [])
	{
		$this->body = $body;
		$this->statusCode = (int) $status;
		$this->headers = $headers;
		if (!isset($this->headers['Content-Type'])) {
			$this->headers['Content-Type'] = 'text/html';
		}
	}

	public function send()
	{
		$this->sendHeaders();
		$this->sendBody();
	}

	protected function sendHeaders()
	{
		// status
		http_response_code($this->statusCode);
		// headers
		foreach ($this->headers as $name => $value) {
			header($name . ': ' . $value, true, $this->statusCode);
		}
	}

	protected function sendBody()
	{
		echo $this->body;
	}
}