<?php
namespace Fatum12\Fileman\Util;


class Path
{
	public static function normalize($path)
	{
		$path = rtrim(str_replace('\\', '/', $path), '/');
		return preg_replace('#/{2,}#', '/', $path);
	}

	public static function safe($path)
	{
		$path = self::normalize($path);
		$path = preg_replace('#\.\.?/#', '', $path);

		return $path;
	}

	public static function getUniqueFileName($path, $fileName)
	{
		$fileInfo = pathinfo($path . '/' . $fileName);
		$index = 0;
		while (file_exists($path . '/' . $fileName)) {
			$index++;
			$fileName = $fileInfo['filename'] . ' (' . $index . ')';
			if (isset($fileInfo['extension'])) {
				$fileName .= '.' . $fileInfo['extension'];
			}
		}

		return $fileName;
	}

	public static function isValidFileName($fileName)
	{
		return preg_match('/^[^\/\?\*:;{}\\\]+$/', $fileName);
	}
}