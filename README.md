# Fileman

Simple file manager. Work in progress. Not ready for production.

## [Demo](http://odd.su/projects/fileman/examples/widget.html)

![Screenshot](examples/images/preview.png)

## Futures

* Upload files
* Download selected file
* Create directories
* Rename file/directory
* Delete selected files/directories
* Copy/move files
* Extract archives
* Create zip archives
* Preview images, text files and archives
* Resize images
* Embedding as widget and popup window
* and others